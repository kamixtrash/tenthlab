import org.junit.BeforeClass;
import org.junit.Test;
import stream.MyStream;

import java.util.ArrayList;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class MyStreamTests {

    static ArrayList<String> firstStringArrayList = new ArrayList<>();
    static ArrayList<String> secondStringArrayList = new ArrayList<>();
    static ArrayList<Integer> integerArrayList = new ArrayList<>();
    static MyStream<String> stringMyStream;
    static MyStream<Integer> integerMyStream;

    @BeforeClass
    public static void prepareData() {
        for (int i = 0; i < 10; i++) {
            if (i % 2 == 0) {
                firstStringArrayList.add("foo");
                secondStringArrayList.add("bar");
                integerArrayList.add(i * i);
            } else {
                firstStringArrayList.add("bar");
                secondStringArrayList.add("foo");
                integerArrayList.add(i);
            }
        }
    }

    @Test
    public void emptyStreamTest() {
        assertEquals(0, MyStream.empty().count());
    }

    @Test
    public void createStreamOfTest() {
        assertEquals(10, MyStream.of(firstStringArrayList).count());
    }

    @Test
    public void streamFilterTest() {
        stringMyStream = MyStream.of(firstStringArrayList);
        assertEquals(5, stringMyStream.filter(x -> x.equals("foo")).count());
    }

    @Test
    public void streamMapTest() {
        integerMyStream = MyStream.of(integerArrayList);
        Optional<Integer> optional = integerMyStream.map(x -> x *= 3).filter(x -> x > 10).findFirst();
        assertTrue(optional.isPresent());
    }

    @Test
    public void streamDistinctTest() {
        stringMyStream = MyStream.of(secondStringArrayList);
        assertEquals(2, stringMyStream.distinct().count());
    }

    @Test
    public void streamLimitTest() {
        stringMyStream = MyStream.of(secondStringArrayList);
        assertEquals(3, stringMyStream.limit(3).count());
    }

    @Test
    public void streamToListTest() {
        integerMyStream = MyStream.of(integerArrayList);
        ArrayList<Integer> result = (ArrayList<Integer>) integerMyStream.filter(x -> x > 5).toList();
        result.forEach(System.out::println);
    }

    @Test
    public void streamMatchAnyTest() {
        stringMyStream = MyStream.of(firstStringArrayList);
        assertTrue(stringMyStream.anyMatch(x -> x.equals("foo")));
    }

    @Test
    public void streamMatchAllTest() {
        stringMyStream = MyStream.of(secondStringArrayList);
        assertTrue(stringMyStream.filter(x -> x.equals("bar")).allMatch(x -> x.equals("bar")));
    }

    @Test
    public void streamMatchNoneTest() {
        stringMyStream = MyStream.of(firstStringArrayList);
        assertTrue(stringMyStream.noneMatch(x -> x.equals("hello")));
    }

    @Test
    public void streamFindFirstTest() {
        stringMyStream = MyStream.of(secondStringArrayList);
        assertEquals("bar", stringMyStream.findFirst().get());
    }
}
