package stream;

import java.util.Iterator;

/**
 * Simple implementation of Iterator which we can get from iterable.
 * @param <T>           type of elements in Iterator.
 */
public class SimpleIterator<T> implements Iterator<T> {
    private final Iterable<? extends T> iterable;
    private Iterator<? extends T> iterator;

    public SimpleIterator(Iterable<? extends T> iterable) {
        this.iterable = iterable;
    }

    /**
     * If we don't have any Iterator yet, we get it from Iterable we've got from constructor.
     */
    private void ensureIterator() {
        if (iterator != null) {
            return;
        }
        iterator = iterable.iterator();
    }

    @Override
    public boolean hasNext() {
        ensureIterator();
        return iterator.hasNext();
    }

    @Override
    public T next() {
        ensureIterator();
        return iterator.next();
    }

    @Override
    public void remove() {
        ensureIterator();
        iterator.remove();
    }
}
