package utils;

import com.sun.istack.internal.NotNull;

import java.util.HashSet;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Set;

/**
 * Class for performing Distinct operation.
 * @param <T>           type of elements in Stream.
 */
public class MyDistinct<T> implements Iterator<T> {
    private final Iterator<? extends T> iterator;
    private final Set<T> set;
    protected T next;
    protected boolean hasNext, isInit;

    public MyDistinct(@NotNull Iterator<? extends T> iterator) {
        this.iterator = iterator;
        this.set = new HashSet<>();
    }

    public void nextIteration() {
        while (hasNext = iterator.hasNext()) {
            next = iterator.next();
            if (set.add(next)) {
                return;
            }
        }
    }

    @Override
    public boolean hasNext() {
        if(!isInit) {
            nextIteration();
            isInit = true;
        }
        return hasNext;
    }

    @Override
    public T next() {
        if (!isInit) {
            hasNext();
        }
        if (!hasNext) {
            throw new NoSuchElementException();
        }
        final T result = next;
        nextIteration();
        if (!hasNext) {
            next = null;
        }
        return result;
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException("remove not supported.");
    }
}
