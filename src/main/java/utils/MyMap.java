package utils;

import com.sun.istack.internal.NotNull;

import java.util.Iterator;
import java.util.function.Function;

/**
 * Class for performing Map operation.
 * @param <T>           type of elements in current Stream.
 * @param <R>           type of elements in resulting Stream.
 */
public class MyMap<T, R> implements Iterator<R> {
    private final Iterator<? extends T> iterator;
    private final Function<? super T, ? extends R> mapper;

    public MyMap(@NotNull Iterator<? extends T> iterator,
                 @NotNull Function<? super T, ? extends R> mapper) {
        this.iterator = iterator;
        this.mapper = mapper;
    }

    @Override
    public void remove() {
        iterator.remove();
    }

    @Override
    public boolean hasNext() {
        return iterator.hasNext();
    }

    @Override
    public R next() {
        return mapper.apply(iterator.next());
    }
}
