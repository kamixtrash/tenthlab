package stream;

import com.sun.istack.internal.NotNull;
import utils.MyDistinct;
import utils.MyFilter;
import utils.MyLimit;
import utils.MyMap;

import java.io.Closeable;
import java.io.IOException;
import java.util.*;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

/**
 * Simple implementation of Stream.
 * @param <T>               type of class which will be in Stream.
 */
public class MyStream<T> implements Closeable {

    /**
     * Iterator for all operations.
     */
    private final Iterator<? extends T> iterator;

    /**
     * Constructor for getting Iterator from Iterable.
     * @param iterable      some collection.
     */
    private MyStream(Iterable<? extends T> iterable) {
        this(new SimpleIterator<T>(iterable));
    }

    /**
     * Constructor with already prepared Iterator.
     * @param iterator      already prepared Iterator.
     */
    public MyStream(Iterator<? extends T> iterator) {
        this.iterator = iterator;
    }

    /**
     * Method for creating an empty Stream.
     * @param <T>           parametrized type.
     * @return              empty Stream of selected type.
     */
    public static <T> MyStream<T> empty() {
        return of(Collections.emptyList());
    }

    /**
     * Method for creating Stream from a collection (List).
     * @param someList      List from which we want to create a Stream.
     * @param <T>           parametrized type.
     * @return              Stream from List.
     */
    public static <T> MyStream<T> of(List<T> someList) {
        return new MyStream<>(someList);
    }

    /**
     * Non-terminal operation for filtering values in Stream.
     * @param predicate     some predicate which will be called for each element.
     * @return              filtered Stream.
     */
    public MyStream<T> filter(@NotNull final Predicate<? super T> predicate) {
        return new MyStream<T>(new MyFilter<T>(iterator, predicate));
    }

    /**
     * Non-terminal operation for mapping Stream.
     * @param mapper        function which will be performed for every element.
     * @param <R>           the type of elements in resulting Stream.
     * @return              new Stream.
     */
    public <R> MyStream<R> map(@NotNull final Function<? super T, ? extends R> mapper) {
        return new MyStream<R>(new MyMap<T, R>(iterator, mapper));
    }

    /**
     * Non-terminal operation that returns Stream with first maxSize elements.
     * @param maxSize       amount of elements that should be left in Stream.
     * @return              new Stream.
     */
    public MyStream<T> limit(final long maxSize) {
        if (maxSize < 0) {
            throw new IllegalArgumentException("maxSize can't be negative.");
        }
        if (maxSize == 0) {
            return MyStream.empty();
        }
        return new MyStream<T>(new MyLimit<T>(iterator, maxSize));
    }

    /**
     * Non-terminal operation for getting new Stream with distinct elements.
     * @return              new Stream.
     */
    public MyStream<T> distinct() {
        return new MyStream<T>(new MyDistinct<T>(iterator));
    }

    /**
     * Terminal operation for counting all elements in current Stream.
     * @return              amount of elements.
     */
    public long count() {
        long count = 0;
        while (iterator.hasNext()) {
            iterator.next();
            count++;
        }
        return count;
    }

    /**
     * Terminal operation for collecting elements from Stream to List.
     * @return              List with elements from Stream.
     */
    public List<T> toList() {
        final List<T> result = new ArrayList<>();
        while (iterator.hasNext()) {
            result.add(iterator.next());
        }
        return result;
    }

    /**
     * Terminal operation for finding any element that matches some predicate.
     * @param predicate     predicate that will be performed for every element.
     * @return              true if at least one element in Stream satisfies predicate.
     */
    public boolean anyMatch(@NotNull Predicate<? super T> predicate) {
        return match(predicate, MATCH_ANY);
    }

    /**
     * Terminal operation for finding out if all elements matches some predicate.
     * @param predicate     predicate that will be performed for every element.
     * @return              true if all elements in Stream satisfies predicate.
     */
    public boolean allMatch(@NotNull Predicate<? super T> predicate) {
        return match(predicate, MATCH_ALL);
    }

    /**
     * Terminal operation for finding out if none of elements matches some predicate.
     * @param predicate     predicate that will be performed for every element.
     * @return              true if none of elements in Stream satisfies predicate.
     */
    public boolean noneMatch(@NotNull Predicate<? super T> predicate) {
        return match(predicate, MATCH_NONE);
    }

    private static final int MATCH_ANY = 0;
    private static final int MATCH_ALL = 1;
    private static final int MATCH_NONE = 2;

    private boolean match(Predicate<? super T> predicate, int matchKind) {
        final boolean kindAny = matchKind == MATCH_ANY;
        final boolean kindAll = matchKind == MATCH_ALL;

        while (iterator.hasNext()) {
            final T value = iterator.next();
            final boolean match = predicate.test(value);
            if (match ^ kindAll) {
                return kindAny && match;
            }
        }
        return !kindAny;
    }

    /**
     * Terminal operation for getting first element.
     * @return              Optional that can be empty or contain first element of Stream.
     */
    public Optional<T> findFirst() {
        if (iterator.hasNext()) {
            return Optional.of(iterator.next());
        }
        return Optional.empty();
    }

    /**
     * Terminal operation that performs some action for every element in Stream.
     * @param action        the action to be performed on every element.
     */
    public void forEach(final Consumer<? super T> action) {
        while (iterator.hasNext()) {
            action.accept(iterator.next());
        }
    }

    @Override
    public void close() throws IOException {
        throw new UnsupportedOperationException("sorry.");
    }
}
