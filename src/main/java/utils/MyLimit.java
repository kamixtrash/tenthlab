package utils;

import com.sun.istack.internal.NotNull;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Class for performing Limit operations.
 * @param <T>           type of elements in Stream.
 */
public class MyLimit<T> implements Iterator<T> {
    private final Iterator<? extends T> iterator;
    private final long maxSize;
    private long index;

    public MyLimit(@NotNull Iterator<? extends T> iterator,
                   long maxSize) {
        this.iterator = iterator;
        this.maxSize = maxSize;
        this.index = 0;
    }

    @Override
    public boolean hasNext() {
        return (index < maxSize) && iterator.hasNext();
    }
    @Override
    public void remove() {
        throw new UnsupportedOperationException("remove not supported");
    }

    @Override
    public final T next() {
        if (!hasNext()) {
            throw new NoSuchElementException();
        }
        return nextIteration();
    }

    public T nextIteration() {
        index++;
        return iterator.next();
    }
}
